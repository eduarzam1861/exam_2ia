"""
    Author :: Eduardo Zamora
    GitLab :: https://gitlab.com/eduarzam1861/exam_2ia.git
    Version :: 0.0.1
"""
from unipath import Path

def createdFile (nameFile):
    file = open('Data/'+nameFile+'.txt', 'w')
    file.close()


def openFile (nameFile, words):
    file = open('Data/'+nameFile+'.txt', 'a+')
    file.write(words + '\n')
    file.close()

def existWord (nameFile, words):
    with open('Data/'+nameFile+'.txt') as myfile:
         for line in myfile:
             if words == line.strip():
                 return True

    return False



def addWord (nameFile, words):
    f = Path('Data/fileWords.txt')
    if f.exists():
        openFile(nameFile, words)
    else:
        createdFile(nameFile)
        openFile(nameFile, words)


def game (fileName):
    while True:
        print('Agregra palabra :')
        palabra = input()
        if existWord(fileName, palabra):
            print("Esta palabra ya existe, Deseas agregar otra? y/n")
            res = input()
            if res == 'n':
                break
        else:
            addWord(fileName, palabra)
            print("Palabra "+palabra+" agregada")
            print("Desea agregar otra palabra ? s/n")
            res = input()
            if res == 'n':
                break

if __name__ == '__main__':
    game('data_words')



